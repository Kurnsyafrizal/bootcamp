import java.util.Scanner;
public class Bintang{
    public static void main(String args[]) {
        System.out.println(segi3("*", 10)); //input simbol dan tinggi segitiga
    }

    public static String segi3(String symbol, int side) { //pemanggilan command dari input diatas
        StringBuilder str = new StringBuilder();

        // bagian atas segitiga
        for (int i = side; i > 0; i--){
            str.append(symbol + " ");
        }
        str.append("\n");

        // bagian samping
        for (int i = side-2; i > 0; i--){
            //Samping kiri 
            for (int j = 1; j < side-i; j++) {
                str.append(" ");
            }
            str.append(symbol);

            // samping kanan
            for (int k = 1; k <= (i * 2)- 1 ; k++) {
                str.append(" ");
            }
            str.append(symbol).append("\n");
        }

        // bagian bawah
        for (int i = side-1; i > 0; i--) {
            str.append(" ");
        }
        return str.append(symbol).append("\n").toString();
    }
}